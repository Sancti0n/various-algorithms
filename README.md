# Various Algorithms
Some algorithms I had to write depending on the situation


[![forthebadge made-with-python](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)

## Functions :

- allCombinations
- mathCombinations


## License
[MIT](https://choosealicense.com/licenses/mit/)